Install git  
https://git-scm.com/book/en/v2/Getting-Started-Installing-Git  
for your system

Install Java 8  
https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html  
click the agree radio button  
select the latest version that matches your system  
install

Install gradle  
https://gradle.org/install/  
select the version that matches your system  
install

https://bitbucket.org/cdrchops/cherokee-alignment-joiner  
I need to add some arguments still - 

Put your xlsx files in the data directory inside the project then in the root directory command prompt type "gradle runScript"   
The program will go through every xslx in the data directory and make a tab (not comma) delimited file and output it as eng_chr.txt in the data directory

Todo:  
* add arguments for:  
    * data directory  
    * output directory  
    * output file  
    * delimiter \t or ,