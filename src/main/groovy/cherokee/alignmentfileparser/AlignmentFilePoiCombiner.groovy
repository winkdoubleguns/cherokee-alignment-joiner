package cherokee.alignmentfileparser

import groovy.io.FileType
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook

//args
// data directory
// output directory
// output file
// delimiter \t or ,

public static void processXls() {
//        String arg1 = args[0];
    def f = new File("data/")
    def report = new File("data/eng_chr.txt");
    report.write("", "UTF-8")

    f.eachFile(FileType.FILES) { file ->
        //ignores .DS_Store and the generated eng_chr.txt file
        if (file.name.startsWith(".") || file.name == "eng_chr.txt") {
            return
        }

        //Create Workbook instance holding reference to .xlsx file
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);

        Iterator<Row> rowIterator = sheet.iterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
              writeRowToReport(row, report);
        }
    }
}

public static void writeRowToReport(Row row, File f) {
    //For each row, iterate through all the columns
    Iterator<Cell> cellIterator = row.cellIterator();

    def eng
    def chr

    while (cellIterator.hasNext()) {
        Cell cell = cellIterator.next();
        int columnIndex = cell.getColumnIndex();

        if (columnIndex == 0) {
            eng = ""
            chr = ""
            if (cell.getStringCellValue().trim() == '') {
                return;
            }
        }


        CellType cellType = cell.getCellType();
        String stringValue = "";
        switch (cellType) {
            case CellType.NUMERIC:
                stringValue = String.valueOf(cell.getNumericCellValue());
                break;
            case CellType.ERROR:
                stringValue = String.valueOf(cell.getErrorCellValue());
                break;
            case CellType.STRING:
                stringValue = cell.getStringCellValue();
                break;
        }

        if (columnIndex == 0) {
            eng = stringValue
        } else {
            chr = stringValue
            //if the cell on the left or the cell on the right don't have any value then don't include them because we need parity
            // also if the cherokee string contains the word 'cherokee' we don't want to use it since it's a header
            if (eng?.size() > 0 && chr?.size() > 0 && !chr?.toLowerCase().contains("cherokee")) {
                f.append(eng.trim() + "\t" + chr.trim() + "\n", "UTF-8")
            }
        }
    }
}

processXls();
